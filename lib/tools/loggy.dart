import 'dart:io' show Platform;

import 'package:ansicolor/ansicolor.dart';
import 'package:flutter/foundation.dart';
import 'package:loggy/loggy.dart';

import '../veilid_support/veilid_support.dart';

String wrapWithLogColor(LogLevel? level, String text) {
  // XXX: https://github.com/flutter/flutter/issues/64491
  if (!kIsWeb && Platform.isIOS) {
    return text;
  }

  if (level == null) {
    return text;
  }
  final pen = AnsiPen();
  ansiColorDisabled = false;
  switch (level) {
    case LogLevel.error:
      pen
        ..reset()
        ..red(bold: true);
      return pen(text);
    case LogLevel.warning:
      pen
        ..reset()
        ..yellow(bold: true);
      return pen(text);
    case LogLevel.info:
      pen
        ..reset()
        ..white(bold: true);
      return pen(text);
    case LogLevel.debug:
      pen
        ..reset()
        ..green(bold: true);
      return pen(text);
    case traceLevel:
      pen
        ..reset()
        ..blue(bold: true);
      return pen(text);
  }
  return text;
}

extension PrettyPrintLogRecord on LogRecord {
  String pretty() {
    final lstr =
        wrapWithLogColor(level, '[${level.toString().substring(0, 1)}]');
    return '$lstr $message';
  }
}

class CallbackPrinter extends LoggyPrinter {
  CallbackPrinter() : super();

  void Function(LogRecord)? callback;

  @override
  void onLog(LogRecord record) {
    debugPrint(record.pretty());
    callback?.call(record);
  }

  void setCallback(void Function(LogRecord)? cb) {
    callback = cb;
  }
}

CallbackPrinter globalTerminalPrinter = CallbackPrinter();

LogOptions getLogOptions(LogLevel? level) => LogOptions(
      level ?? LogLevel.all,
      stackTraceLevel: LogLevel.error,
    );

class RootLoggy implements LoggyType {
  @override
  Loggy<RootLoggy> get loggy => Loggy<RootLoggy>('');
}

Loggy get log => Loggy<RootLoggy>('veilidchat');

void initLoggy() {
  Loggy.initLoggy(
    logPrinter: globalTerminalPrinter,
    logOptions: getLogOptions(null),
  );

  // ignore: do_not_use_environment
  const isTrace = String.fromEnvironment('LOG_TRACE') != '';
  LogLevel logLevel;
  if (isTrace) {
    logLevel = traceLevel;
  } else {
    logLevel = kDebugMode ? LogLevel.debug : LogLevel.info;
  }

  Loggy('').level = getLogOptions(logLevel);
}
